<?php
if (!defined('TYPO3')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'drk_clothescontainersearch',
    'ClothescontainerSearch',
    [
        \DRK\DrkClothescontainersearch\Controller\ClothescontainerSearchController::class => 'form',
    ],
    // non-cacheable actions
    [
        \DRK\DrkClothescontainersearch\Controller\ClothescontainerSearchController::class => 'form',
    ],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'drk_clothescontainersearch',
    'ClothescontainerSearchResult',
    [
        \DRK\DrkClothescontainersearch\Controller\ClothescontainerSearchResultController::class => 'clothescontainerResult',
    ],
    // non-cacheable actions
    [
        \DRK\DrkClothescontainersearch\Controller\ClothescontainerSearchResultController::class => 'clothescontainerResult',
    ],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);


