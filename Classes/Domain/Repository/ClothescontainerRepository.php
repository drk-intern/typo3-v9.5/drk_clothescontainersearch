<?php

namespace DRK\DrkClothescontainersearch\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use DRK\DrkGeneral\Domain\Repository\AbstractDrkRepository;
use DRK\DrkGeneral\Utilities\Utility;
use TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException;

class ClothescontainerRepository extends AbstractDrkRepository
{


    /**
     * get clothescontainer by zip code
     *
     * @param     $sSearchstring
     * @param int $iRange
     *
     * @return array
     * @throws \Exception
     */
    public function getClothescontainersByZip($sSearchstring, $iRange) {
        return $this->__getClothescontainers($sSearchstring, $iRange, 'zip');
    }

    /**
     * get clothescontainer by geo coordninate
     *
     * @param     $sSearchstring
     * @param int $iRange
     *
     * @return array
     * @throws \Exception
     */
    public function getClothescontainersByCoord($sSearchstring, $iRange) {
        return $this->__getClothescontainers($sSearchstring, $iRange, 'coord');
    }

    /**
     * get clothescontainer by zip code or coordinate
     *
     * @param        $sZipCode
     * @param int    $iRange
     * @param string $by (zip or coord)
     *
     * @return array
     * @throws \Exception
     */
    protected function __getClothescontainers($sSearchstring, $iRange = 5, $by = 'zip')
    {
        $cacheIdentifier = sha1(json_encode(['drk_clothescontainersearch|__getClothescontainers', $sSearchstring, $iRange, $by]));
        $result = $this->getCache()->get($cacheIdentifier);

        if (($result !== null) && ($result !== false)) {
            return $result;
        }

        if ($by == 'coord') {
            $params = array(
                'location' => $sSearchstring,
                'range' => $iRange
            );
        } else {
            $params = array(
                'zipcode' => $sSearchstring,
                'range' => $iRange
            );
        }

        $this->getJsonClient('old');

        $response = $this->jsonClient->getclothcontainerlist($params);

        if (!empty($response)) {
            if (strtolower($response->status) != "ok") {
                throw new \Exception("Der Webservice meldet: " . $response->message);
            } else {
                $aResult = $response->results;
            }
        } else {
            throw new \Exception('Webservice antwortet nicht oder fehlerhaft!');
            return [];
        }

        // if no result, exit
        if (empty($aResult) || !is_array($aResult)) {
            return [];
        }

        // cleanup CC array
        $aCC = array();
        $latLngArray = [];
        $latLngCounter = 0;

        foreach ($aResult as $oCCtmp) {
            // without coordinations, not usable
            if ($oCCtmp->containerlatitude > 0 && $oCCtmp->containerlongitude > 0 && $oCCtmp->containerid > 0) {
                $aCC[] = $oCCtmp;
            }
        }

        // sort clothescontainers by distance
        usort($aCC, function ($a, $b) {
            return $a->distance <=> $b->distance;
        });


        //now set offer at the end
        foreach ($aResult as $oCCtmp) {
            // without coordinations, not usable
            if ($oCCtmp->offerlatitude > 0 && $oCCtmp->offerlongitude > 0 && $oCCtmp->containerid == 0) {
                $aCC[] = $oCCtmp;
            }
        }

        $iteration = 1;
        foreach ($aCC as $iCCkey => $aCCtmp) {
            $curseGeoHash = $aCCtmp->containerlatitude . ':' . $aCCtmp->containerlongitude;
            if (($latLngIndex = array_search($curseGeoHash, $latLngArray)) !== false) {
                $currentAsciiChar = $latLngIndex;
            } else {
                $latLngArray[] = $curseGeoHash;
                $currentAsciiChar = $latLngCounter++;
            }

            $letter = chr($currentAsciiChar + 65);

            $aCC[$iCCkey]->markerLabel = str_repeat($letter, $iteration);

            // At the end of the alphabet, start again from the beginning.
            if ($latLngCounter == 26) {
                $iteration ++;
                $latLngCounter = 0;
            }
        }

        $this->getCache()->set($cacheIdentifier, $aCC);

        return $aCC;
    }


    /**
     * @param string $where
     * @param string $sortingBy
     * @param string $orderBy
     *
     * @return array
     * @throws NoSuchCacheException
     */
    public function getOrganisation(string $where, string $sortingBy = '', string $orderBy = ''): array
    {
        $cacheIdentifier = sha1(json_encode(['drk_clothescontainersearch|getOrganisation', $where, $sortingBy, $orderBy]));
        $result = $this->getCache()->get($cacheIdentifier);

        if (($result !== null) && ($result !== false)) {
            return $result;
        }

        $result =  Utility::convertObjectToArray(
            $this->getDldbClient()->getOrganisationbyZiporCity($where, $sortingBy, $orderBy)
        );

        $this->getCache()->set($cacheIdentifier, $result, [], $this->heavy_cache);

        return $result;

    }
}
