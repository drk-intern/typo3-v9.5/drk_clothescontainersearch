<?php

namespace DRK\DrkClothescontainersearch\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use DRK\DrkGeneral\Domain\Repository\AbstractRepository;
use DRK\DrkGeneral\Utilities\Utility;

use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Service\TypoScriptService;

class OrganisationRepository extends AbstractRepository
{

    /**
     * @param string $sword
     * @return array
     * @throws \Exception
     */
    public function findByZipOrCity($sword)
    {
        $cacheIdentifier = sha1(json_encode(['drk_clothescontainersearch|findByZipOrCity', $sword]));
        $result = $this->getCache()->get($cacheIdentifier);

        if (($result !== null) && ($result !== false)) {
            return $result;
        }

        $organisations = Utility::convertObjectToArray(
            $this->getClient()->getOrganisationbyZiporCity($sword)
        );

        if (empty($organisations)) {
            return [];
        }

        $this->getCache()->set($cacheIdentifier, $organisations);

        return $organisations;
    }

    /**
     * @param string $sortBy
     * @param string $orderBy
     * @return array
     */
    public function findOfferList($sortBy = 'description', $orderBy = 'asc')
    {
        $cacheIdentifier = sha1(json_encode(['drk_clothescontainersearch|findOfferList', $sortBy, $orderBy]));
        $result = $this->getCache()->get($cacheIdentifier);

        if (($result !== null) && ($result !== false)) {
            return $result;
        }

        $offers = Utility::convertObjectToArray(
            $this->getClient()->getOfferDescriptionList($sortBy, $orderBy)
        );

        if (empty($offers)) {
            return [];
        }

        $this->getCache()->set($cacheIdentifier, $offers);

        return $this->filterOffersByOfferClass($offers);
    }

    /**
     * @param $offerId
     * @param $zip
     * @param int $range
     * @return array
     */
    public function findOfferLinksByZipOrCity($offerId, $zip, $range = 0)
    {
        $cacheIdentifier = sha1(json_encode(['drk_clothescontainersearch|findOfferLinksByZipOrCity', $offerId, $zip, $range]));
        $result = $this->getCache()->get($cacheIdentifier);

        if (($result !== null) && ($result !== false)) {
            return $result;
        }

        $offerLinks = Utility::convertObjectToArray(
            $this->getClient()->getOrganisationOfferLinksbyZiporCityTracked($zip, $offerId, $range)
        );

        if (empty($offerLinks)) {
            return [];
        }

        foreach ($offerLinks as &$offerLink) {
            if (empty($offerLink['orgOfferName'])) {
                $offerLink['orgOfferName'] = $offerLink['orgOfferDescription'];
            }
            if (empty($offerLink['offerZipcode'])) {
                $offerLink['offerStreet'] = $offerLink['Organisation']['orgStreet'];
                $offerLink['offerZipcode'] = $offerLink['Organisation']['orgZipcode'];
                $offerLink['offerCity'] = $offerLink['Organisation']['orgCity'];
                $offerLink['orgOfferLatitude'] = $offerLink['Organisation']['orgLatitude'];
                $offerLink['orgOfferLongitude'] = $offerLink['Organisation']['orgLongitude'];
            }
            if (empty($offerLink['orgOfferURL'])) {
                $offerLink['orgOfferURL'] = $offerLink['orgOfferTypeURL'];
            }
        }

        usort($offerLinks, function ($a, $b) {
            return ($a['distance'] == $b['distance']) ? 0 : (($a['distance'] < $b['distance']) ? -1 : 1);
        });

        $this->getCache()->set($cacheIdentifier, $offerLinks);

        return $offerLinks;
    }

    /**
     * ItemsProcFunc for OrganisationPlugin->settings.flexforms.preselection
     *
     * @param array $parameters
     * @return array
     */
    public function addOfferListItems(array $parameters)
    {
        if (empty($parameters['config']['itemsProcFunc_params'])
            || empty($parameters['config']['itemsProcFunc_params']['extensionName'])
            || empty($parameters['config']['itemsProcFunc_params']['pluginName'])
        ) {
            throw new \InvalidArgumentException('Missing itemsProcFunc configuration.', 1455789129533);
        }

        $configuration = $parameters['config']['itemsProcFunc_params'];
        /** @var ConfigurationManagerInterface $configurationManager */
        $configurationManager = GeneralUtility::makeInstance(ConfigurationManagerInterface::class);
        $configurationManager->setConfiguration($configuration);
        $setup =
            $configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);

        // Get current TypoScript settings
        $settings = [];
        $typoScriptService = GeneralUtility::makeInstance(TypoScriptService::class);
        if (is_array($setup['plugin.']['tx_' . strtolower($configuration['extensionName']) . '.']['settings.'])) {
            $settings = $typoScriptService->convertTypoScriptArrayToPlainArray(
                $setup['plugin.']['tx_' . strtolower($configuration['extensionName']) . '.']['settings.']
            );
        }
        $pluginSignature = strtolower($configuration['extensionName'] . '_' . $configuration['pluginName']);
        if (is_array($setup['plugin.']['tx_' . $pluginSignature . '.']['settings.'])) {
            ArrayUtility::mergeRecursiveWithOverrule(
                $settings,
                $typoScriptService->convertTypoScriptArrayToPlainArray(
                    $setup['plugin.']['tx_' . $pluginSignature . '.']['settings.']
                )
            );
        }
        $this->settings = $settings;

        foreach ($this->findOfferList() as $offer) {
            $parameters['items'][] = [
                $offer['orgOfferDescription'],
                $offer['orgOfferId'],
                '',
            ];
        }

        return $parameters;
    }

    /**
     * @param array $offers
     * @param string $offerClass
     *
     * @return array
     */
    public function filterOffersByOfferClass(array $offers, $offerClass = 'K')
    {
        $filteredOffers = [];
        foreach ($offers as $offer) {
            if ($offer['orgOfferClass'] == $offerClass) {
                $filteredOffers[] = $offer;
            }
        }
        return $filteredOffers;
    }
}
