<?php

namespace DRK\DrkClothescontainersearch\ViewHelpers;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Page\AssetCollector;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

//TODO: reconsider use of Google API key
class GoogleMapViewHelper extends AbstractViewHelper
{
    /**
     * @var array
     */
    protected $settings;

    /**
     * @var bool
     */
    protected $escapeOutput = false;

    /**
     * @var string
     */
    protected $googleApiKey;

    /**
     * @var string
     */
    protected $googleMapsApiUrl;

    /**
     * @param AssetCollector $assetCollector
     */
    public function __construct(
        protected readonly AssetCollector $assetCollector
    ) {
    }

    /**
     * Initialize arguments
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArguments([
            ['locations', 'string', 'The locations to show on the map', true, null],
            ['latitudeField', 'string', 'The identifier for latitude', false, 'Latitude'],
            ['longitudeField', 'string', 'The identifier for longitude', false, 'Longitude'],
            ['height', 'string', 'The height', false, 440],
            ['width', 'string', 'The width', false, null],
            ['class', 'string', 'The class', false, null],
            ['alt', 'string', 'The alt', false, null],
            ['title', 'string', 'The title', false, null]
        ]);
    }

    /**
     * @param array $arguments
     *
     * @throws \TYPO3\CMS\Fluid\Core\ViewHelper\Exception
     */
    protected function registerArguments(array $arguments)
    {
        if (is_array($arguments)) {
            foreach ($arguments as $argument) {
                if (is_array($argument)) {
                    $this->registerArgument($argument[0], $argument[1], $argument[2], $argument[3], $argument[4]);
                }
            }
        }
    }

    /**
     * @return array
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * @param array $settings
     */
    public function setSettings($settings)
    {
        $this->settings = $settings;
    }

    /**
     * @return string
     */
    public function getGoogleApiKey()
    {
        return $this->googleApiKey;
    }

    /**
     * @param string $googleApiKey
     */
    public function setGoogleApiKey($googleApiKey)
    {
        $this->googleApiKey = $googleApiKey;
    }

    /**
     * @return string
     */
    public function getGoogleMapsApiUrl()
    {
        return $this->googleMapsApiUrl;
    }

    /**
     * @param string $googleMapsApiUrl
     */
    public function setGoogleMapsApiUrl($googleMapsApiUrl)
    {
        $this->googleMapsApiUrl = $googleMapsApiUrl;
    }

    /**
     * @param array $additionalOptions
     *
     * @return string
     */
    protected function generateGoogleMapsApiScriptUrl(array $additionalOptions = []):string
    {
        if (!empty($this->getGoogleMapsApiUrl())/* && !empty($this->getGoogleApiKey())*/) {
            $additionalOptionsArray = [];
            if (!empty($additionalOptions)) {
                foreach ($additionalOptions as $k => $v) {
                    $additionalOptionsArray[] = $k . '=' . $v;
                }
            }
            return str_replace('?', '', $this->getGoogleMapsApiUrl()) .
                '?' . ($this->getGoogleApiKey() ? 'key=' . $this->getGoogleApiKey() . '&' : '') .
                implode('&', $additionalOptionsArray);
        }
        return '';
    }

    /**
     * @return string
     * @throws \TYPO3\CMS\Fluid\Core\ViewHelper\Exception\InvalidVariableException
     */
    public function render()
    {
        $this->setSettings($this->templateVariableContainer->get('settings'));
        $this->setGoogleApiKey($this->settings['google_api_key']);
        $this->setGoogleMapsApiUrl($this->settings['google_maps_api_url']);

        $this->assetCollector->addStyleSheet(
            'drk_clothescontainersearch-googlemaps-css',
            'EXT:drk_clothescontainersearch/Resources/Public/Css/googlemaps.css'
        );

        if ($this->settings['GDPR']) {
            $this->assetCollector->addInlineJavaScript(
                "drk_clothescontainersearch-googlemaps-gdpr",
                "
                    $('#googlemaps_gdpr').click(function(){
                            $.getScript('/" .
                PathUtility::getPublicResourceWebPath (
                    'EXT:drk_clothescontainersearch/Resources/Public/Scripts/googlemaps.js'
                )
                . "');
                            $('#googlemaps_consent').hide();
                           });
                ",
                [],
                ['priority' => false]
            );
        } else {
            $this->assetCollector->addJavaScript(
                'drk_clothescontainersearch-googlemaps',
                'EXT:drk_clothescontainersearch/Resources/Public/Scripts/googlemaps.js',
                [],
                ['priority' => false]
            );
        }

        $mapHtml = '';
        if (/*!empty($this->getGoogleApiKey()) && */ !empty($this->arguments['locations'])) {
            $mapHtml = '<div id="map" style="height:' . $this->arguments['height'] . 'px; width:' . $this->arguments['width'] . 'px;">';
            if ($this->settings['GDPR']) {
                $mapHtml .= '<div id="googlemaps_consent">
                <div><input class="button o-btn" id="googlemaps_gdpr" type="button" value="Karte anzeigen"></div>
                <div>Wenn Sie die Karte nutzen, werden Cookies aktiviert, die für die Nutzung von Google Maps nötig sind. Diese Cookies übertragen einzelne Nutzerdaten an Google Maps. Mit der Nutzung der Karte erklären Sie sich automatisch damit einverstanden.</div>
                </div>';
            }
            $mapHtml .= '</div>';
            $mapHtml .= '<script type="text/javascript">';

            $mapHtml .= "
                let google_maps_src = '" . $this->generateGoogleMapsApiScriptUrl() . "';
                let icon_cc = '" . PathUtility::getPublicResourceWebPath (
                    'EXT:drk_clothescontainersearch/Resources/Public/Images/pic_marker_cc.png') . "';
                let icon_cs = '" . PathUtility::getPublicResourceWebPath (
                    'EXT:drk_clothescontainersearch/Resources/Public/Images/pic_marker_cs.png') . "';
                let icon_w = '" . PathUtility::getPublicResourceWebPath (
                    'EXT:drk_clothescontainersearch/Resources/Public/Images/pic_marker_w.png') . "';
            ";

            $mapHtml .= "let googleImgPath = '".PathUtility::getPublicResourceWebPath (
                    'EXT:drk_clothescontainersearch/Resources/Public/Images/'). "'; ";
            $mapHtml .= '

                let locations = [';

            $locationArray = [];

            foreach ($this->arguments['locations'] as $location) {
                if ($location->containerlatitude > 0 && $location->containerlongitude > 0) {
                    $fLatitude = $location->containerlatitude;
                    $fLongitude = $location->containerlongitude;
                    $sLabel = $location->offertypdescription;
                    $sIconset = "icon_cc";
                    $sContent = "<b>" . $location->offertypdescription . "</b>";
                    $sContent .= "<p>" . htmlspecialchars($location->containerstreet) . ", <br/>" . $location->containerzip . " " . htmlspecialchars($location->containercity);
                    $sContent .= empty($location->containernote) ? "" : ", <br/>" . htmlspecialchars($location->containernote) . "</p>";
                } else {
                    $fLatitude = $location->offerlatitude;
                    $fLongitude = $location->offerlongitude;
                    $sLabel = empty($location->offername) ? $location->offertypdescription : $location->offername;
                    $sIconset = strpos($location->offertypdescription, "kammer") ? "icon_w" : "icon_cs";
                    $sContent = "<b>";
                    $sContent .= empty($location->offername) ? $location->offertypdescription : htmlspecialchars($location->offername);
                    $sContent .= "</b><p>" . htmlspecialchars($location->offerstreet) . ", <br/>" . $location->offerzip . " " . htmlspecialchars($location->offercity) . "</p>";
                }

                $locationArray[] = '[' .
                    '"' . $sLabel . '",' .
                    $fLatitude . ',' .
                    $fLongitude . ',' .
                    '"' . $sIconset . '",' .
                    '"' . $sContent . '"' .
                    ']';
            }
            $mapHtml .= implode(',', $locationArray);

            $mapHtml .= '];';
            $mapHtml .= '</script>';
        }
        return $mapHtml;
    }
}
