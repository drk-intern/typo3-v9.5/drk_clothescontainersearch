<?php

namespace DRK\DrkClothescontainersearch\ViewHelpers;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Page\AssetCollector;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class OpenStreetMapViewHelper extends AbstractViewHelper
{
    /**
     * @var array
     */
    protected $settings;

    /**
     * @var bool
     */
    protected $escapeOutput = false;

    /**
     * @param AssetCollector $assetCollector
     */
    public function __construct(
        protected readonly AssetCollector $assetCollector
    ) {
    }

    /**
     * Initialize arguments
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArguments([
            ['locations', 'string', 'The locations to show on the map', true, null],
            ['latitudeField', 'string', 'The identifier for latitude', false, 'Latitude'],
            ['longitudeField', 'string', 'The identifier for longitude', false, 'Longitude'],
            ['height', 'string', 'The height', false, 440],
            ['width', 'string', 'The width', false, null],
            ['class', 'string', 'The class', false, null],
            ['alt', 'string', 'The alt', false, null],
            ['title', 'string', 'The title', false, null]
        ]);
    }

    /**
     * @param array $arguments
     *
     * @throws \TYPO3\CMS\Fluid\Core\ViewHelper\Exception
     */
    protected function registerArguments(array $arguments)
    {
        if (is_array($arguments)) {
            foreach ($arguments as $argument) {
                if (is_array($argument)) {
                    $this->registerArgument($argument[0], $argument[1], $argument[2], $argument[3], $argument[4]);
                }
            }
        }
    }

    /**
     * @return array
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * @param array $settings
     */
    public function setSettings($settings)
    {
        $this->settings = $settings;
    }

    /**
     * @return string
     * @throws \TYPO3\CMS\Fluid\Core\ViewHelper\Exception\InvalidVariableException
     */
    public function render()
    {
        $this->setSettings($this->templateVariableContainer->get('settings'));
        $this->assetCollector->addStyleSheet(
            'drk_clothescontainersearch-osm-css',
            'EXT:drk_clothescontainersearch/Resources/Public/Css/osm.css'
        );

        $this->assetCollector->addStyleSheet(
            'drk_clothescontainersearch-osm-theme-css',
            'EXT:drk_clothescontainersearch/Resources/Public/Scripts/OpenLayers-2.13.1/theme/default/style.css'
        );

        if ($this->settings['GDPR']) {
            $this->assetCollector->addInlineJavaScript(
                'drk_clothescontainersearch-osm-gdpr',
                "
                  $('#osm_gdpr').click(function(){
                    $.getScript('" . PathUtility::getPublicResourceWebPath (
                        'EXT:drk_clothescontainersearch/Resources/Public/Scripts/osm.js'
                    ) . "');
                    $('#osm_consent').hide();
                  });
                ",
                [],
                ['priority' => false]
            );
        } else {
            $this->assetCollector->addJavaScript(
                'drk-clothescontainersearch-js',
                'EXT:drk_clothescontainersearch/Resources/Public/Scripts/osm.js',
                [],
                ['priority' => false]
            );
        }

        $mapHtml = '';
        $mapWidthStyleString = (int)$this->arguments['width'] > 0 ? 'width:' . (int)$this->arguments['width'] . 'px;' : '';
        $mapHeightStyleString = (int)$this->arguments['height'] > 0 ? 'height:' . (int)$this->arguments['height'] . 'px;' : '';

        $aIconSet = array(
            'icon_cc' => PathUtility::getPublicResourceWebPath (
                    'EXT:drk_clothescontainersearch/Resources/Public/Images/pic_marker_cc.png'),
            'icon_cs' => PathUtility::getPublicResourceWebPath (
                'EXT:drk_clothescontainersearch/Resources/Public/Images/pic_marker_cs.png'),
            'icon_w' => PathUtility::getPublicResourceWebPath (
                'EXT:drk_clothescontainersearch/Resources/Public/Images/pic_marker_w.png'),
        );


        if (!empty($this->arguments['locations'])) {
            $mapHtml = '<div id="map" style="' . $mapHeightStyleString . $mapWidthStyleString . ';" >';
            if ($this->settings['GDPR']) {
                $mapHtml .= '<div id="osm_consent">
                <div><input class="button o-btn" id="osm_gdpr" type="button" value="Karte anzeigen"></div>
                <div>Wenn Sie die Karte nutzen, werden einzelne Nutzerdaten an OpenStreetMap übertragen. Mit der Nutzung der Karte erklären Sie sich automatisch damit einverstanden.</div>
            </div>';
            }

            $mapHtml .= '</div>';
            $mapHtml .= '<script type="text/javascript">';
            $mapHtml .= "let osmPath = '".PathUtility::getPublicResourceWebPath (
                    'EXT:drk_clothescontainersearch/Resources/Public/Scripts/OpenLayers-2.13.1/OpenLayers.js') . "'; ";
            $mapHtml .= "let osmImgPath = '" . PathUtility::getPublicResourceWebPath (
                        'EXT:drk_clothescontainersearch/Resources/Public/Scripts/OpenLayers-2.13.1/img/') .  "'; ";

            $mapHtml .= 'let locations = [';

            $locationArray = [];

            foreach ($this->arguments['locations'] as $location) {
                if ($location->containerlatitude > 0 && $location->containerlongitude > 0) {
                    $fLatitude = $location->containerlatitude;
                    $fLongitude = $location->containerlongitude;
                    $sLabel = $location->offertypdescription;
                    $sIconUrl = $aIconSet['icon_cc'];
                    $sContent = "<b>" . $location->offertypdescription . "</b>";
                    $sContent .= "<p>" . htmlspecialchars($location->containerstreet) . ", <br/>" . $location->containerzip . " " . htmlspecialchars($location->containercity);
                    $sContent .= empty($location->containernote) ? "" : ", <br/>" . htmlspecialchars($location->containernote) . "</p>";
                } else {
                    $fLatitude = $location->offerlatitude;
                    $fLongitude = $location->offerlongitude;
                    $sLabel = empty($location->offername) ? $location->offertypdescription : $location->offername;
                    $sIconUrl = strpos(
                        $location->offertypdescription,
                        "kammer"
                    ) ? $aIconSet['icon_w'] : $aIconSet['icon_cs'];
                    $sContent = "<b>";
                    $sContent .= empty($location->offername) ? $location->offertypdescription : htmlspecialchars($location->offername);
                    $sContent .= "</b><p>" . htmlspecialchars($location->offerstreet) . ", <br/>" . $location->offerzip . " " . htmlspecialchars($location->offercity) . "</p>";
                }

                $locationArray[] = '[' .
                    '"' . $sLabel . '",' .
                    $fLatitude . ',' .
                    $fLongitude . ',' .
                    '"' . $sIconUrl . '",' .
                    '"' . $sContent . '"' .
                    ']';
            }


            $mapHtml .= implode(',', $locationArray);

            $mapHtml .= '];';
            $mapHtml .= '</script>';
            $mapHtml .= '<style>.olMapViewport{z-index:1}</style>';
        }
        return $mapHtml;
    }
}
