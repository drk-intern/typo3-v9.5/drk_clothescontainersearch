<?php
namespace DRK\DrkClothescontainersearch\Updates;

use DRK\DrkGeneral\Updates\AbstractSwitchableControllerActionsPluginUpdater;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;

#[UpgradeWizard('drk_clothescontainersearch-SwitchableControlleractionUpdater')]
class SwitchableControllerActionUpdater extends AbstractSwitchableControllerActionsPluginUpdater
{
    protected const MIGRATION_SETTINGS = [
        [
            'sourceListType' => 'drkclothescontainersearch_clothescontainersearch',
            'switchableControllerActions' => 'Clothescontainer->form',
            'targetListType' => '',
            'targetCtype' => 'drkclothescontainersearch_clothescontainersearch'
        ], [
            'sourceListType' => 'drkclothescontainersearch_clothescontainersearch',
            'switchableControllerActions' => 'Clothescontainer->clothescontainerResult',
            'targetListType' => '',
            'targetCtype' => 'drkclothescontainersearch_clothescontainersearchresult'
        ],
    ];
}
