<?php

namespace DRK\DrkClothescontainersearch\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use DRK\DrkClothescontainersearch\Domain\Repository\ClothescontainerRepository;
use Exception;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException;
use TYPO3\CMS\Core\Pagination\ArrayPaginator;
use GeorgRinger\NumberedPagination\NumberedPagination;

class ClothescontainerSearchResultController extends ClothescontainerSearchAbstractController
{

    /**
     * clothescontainerResult view
     *
     * @param string $sSearchstring
     * @param int $iSearchRange
     * @return ResponseInterface
     * @throws NoSuchCacheException
     */
    public function clothescontainerResultAction(string $sSearchstring = '', int $iSearchRange = 0): ResponseInterface
    {
        $Error = false;
        $clothescontainers = [];
        $organisation = [];
        $_isZip = $this->__isZip($sSearchstring);
        $_isCoordinate = $this->__isCoordinate($sSearchstring);
        $currentPage = $this->request->hasArgument('currentPage') ? (int)$this->request->getArgument('currentPage') : 1;

        $iSearchRange = $iSearchRange == 0 ? $this->settings['search_range'] ?? 5 : $iSearchRange;

        // select search range
        if (isset($this->settings['search_range_selection']) && !empty($this->settings['search_range_selection']))
        {
            $this->view->assign('searchRangeSelection', $this->explodeRangeCsv($this->settings['search_range_selection']));
        } else {
            $this->view->assign('searchRangeSelection', []);
        }

        $this->view->assign('searchRange', $iSearchRange);

        if ($_isZip) {
            $clothescontainers = $this->clothescontainerRepository->getClothescontainersByZip($sSearchstring, $iSearchRange);
            $organisation = $this->clothescontainerRepository->getOrganisation($sSearchstring)[0];
        } else {
            if ($_isCoordinate) {
                $clothescontainers = $this->clothescontainerRepository->getClothescontainersByCoord($sSearchstring,
                    $iSearchRange);
                $organisation = $this->clothescontainerRepository->getOrganisation($sSearchstring)[0];
            } else {
                $Error = true;
            }
        }

        $this->view->assignMultiple([
            'zip' => $sSearchstring,
            'clothescontainers' => $clothescontainers,
            'organisation' => $organisation,
            'Error' => $Error,
        ]);

        /**
         * Set pagination
         */
        $itemsPerPage = 10;
        $maximumLinks = 15;

        $paginator = new ArrayPaginator($clothescontainers, $currentPage, $itemsPerPage);
        $pagination = new NumberedPagination($paginator, $maximumLinks);
        $this->view->assign('pagination', [
            'paginator' => $paginator,
            'pagination' => $pagination
        ]);

        return $this->htmlResponse();
    }

    /**
     * returns true, if a geocoordinate string is given
     * @access private
     * @param $sString
     * @return bool
     */
    protected function __isCoordinate($sString): bool
    {
        return preg_match('/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?),[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/', $sString);
    }

    /**
     * returns true, if a numeric string is given and it's at most 5 and min 3 or
     * when $bStrict is true exactly 5 chars long
     * @access private
     * @param $sString
     * @param $bStrict
     * @return bool
     */
    protected function __isZip($sString, $bStrict = false): bool
    {
        return is_numeric($sString) && ($bStrict ? strlen($sString) == 5 : strlen($sString) <= 5 AND strlen($sString) >= 3);
    }
}
