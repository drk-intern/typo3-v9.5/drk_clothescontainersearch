<?php

namespace DRK\DrkClothescontainersearch\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use DRK\DrkClothescontainersearch\Domain\Repository\ClothescontainerRepository;
use DRK\DrkGeneral\Controller\AbstractDrkController;
use TYPO3\CMS\Core\Page\AssetCollector;

class ClothescontainerSearchAbstractController extends AbstractDrkController
{
    /**
     * @var ClothescontainerRepository
     */
    protected ClothescontainerRepository $clothescontainerRepository;

    /**
     * @param ClothescontainerRepository $clothescontainerRepository
     */
    public function injectClothescontainerRepository(ClothescontainerRepository $clothescontainerRepository): void
    {
        $this->clothescontainerRepository = $clothescontainerRepository;
    }

    /**
     * @param AssetCollector $assetCollector
     */
    public function __construct(
        protected readonly AssetCollector $assetCollector
    ) {
    }

    /**
     * Init
     *
     * @return void
     */
    protected function initializeAction(): void
    {
        parent::initializeAction();

        $this->assetCollector->addStyleSheet(
            'drk-clothescontainersearch-css',
            'EXT:drk_clothescontainersearch/Resources/Public/Css/styles.css'
        );

        $this->assetCollector->addJavaScript(
            'drk-clothescontainersearch-js',
            'EXT:drk_clothescontainersearch/Resources/Public/Scripts/drk_clothescontainersearch.js',
            [],
            ['priority' => false]
        );
    }

    /**
     * @param string $strCsv
     * @return array
     */
    protected function explodeRangeCsv( string $strCsv = ""): array
    {
        $result = [];

        if (empty($strCsv)) {
            return $result;
        }

        $arrayCsv = explode(',', $strCsv);

        foreach ($arrayCsv as $value) {
            $result[$value] = $value . 'km';
        }

        return $result;

    }
}
