$.getScript( osmPath, function ($) {
  'use strict';

  OpenLayers.Lang.setCode('de');

  OpenLayers.ImgPath = osmImgPath;

  let i = 0;

  let fromProjection = new OpenLayers.Projection('EPSG:4326');   // Transform from WGS 1984
  let toProjection = new OpenLayers.Projection('EPSG:900913'); // to Spherical Mercator Projection

  let map = new OpenLayers.Map('map', {
    units: 'm',
    projection: fromProjection,
    displayProjection: toProjection
  });

  // OSM Layer
  let osmLayer = new OpenLayers.Layer.OSM(
    'OpenStreetMap',
    // Official OSM tileset as protocol-independent URLs
    [
      'https://a.tile.openstreetmap.org/${z}/${x}/${y}.png',
      'https://b.tile.openstreetmap.org/${z}/${x}/${y}.png',
      'https://c.tile.openstreetmap.org/${z}/${x}/${y}.png'
    ],
    null);
  map.addLayer(osmLayer);

  let vectorLayer = new OpenLayers.Layer.Vector('Kleidercontainer Vector Layer');
  map.addLayer(vectorLayer);

  let bounds = new OpenLayers.Bounds();

  for (i = 0; i < locations.length; i++) {

    let title = locations[i][0];
    let lat = locations[i][1];
    let lon = locations[i][2];
    let icon = locations[i][3];
    let desc = locations[i][4];

    let feature = new OpenLayers.Feature.Vector(
      new OpenLayers.Geometry.Point(lon, lat).transform(fromProjection, toProjection),
      {
        title: title,
        description: desc
      },
      {externalGraphic: icon, graphicHeight: 32, graphicWidth: 32, graphicXOffset: -32, graphicYOffset: -32}
    );


    vectorLayer.addFeatures(feature);
    let lonlat = new OpenLayers.LonLat(lon, lat).transform(fromProjection, toProjection);
    bounds.extend(lonlat);

  }

  map.zoomToExtent(bounds);

  //Add a selector control to the vectorLayer with popup functions
  let controls = {
    selector: new OpenLayers.Control.SelectFeature(vectorLayer, {onSelect: createPopup, onUnselect: destroyPopup})
  };

  function createPopup(feature) {
    feature.popup = new OpenLayers.Popup.FramedCloud('pop',
      feature.geometry.getBounds().getCenterLonLat(),
      null,
      '<div class="markerContent">' + feature.attributes.description + '</div>',
      null,
      true,
      function () {
        controls['selector'].unselectAll();
      }
    );
    feature.popup.closeOnMove = true;
    map.addPopup(feature.popup);
  }

  function destroyPopup(feature) {
    feature.popup.destroy();
    feature.popup = null;
  }

  map.addControl(controls['selector']);
  controls['selector'].activate();
});

