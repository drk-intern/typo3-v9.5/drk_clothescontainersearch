/**
 *
 * DRK Clothescontainersearch Javascript
 *
 */

  $('.tx-drk-clothescontainersearch .o-btn--geoloaction').click(function (event) {

    event.preventDefault();
    let searchbox = $(this).closest('form').find('.searchbox');

    function success(position) {
      let latitude  = position.coords.latitude;
      let longitude = position.coords.longitude;

      searchbox.val(latitude + ',' + longitude);
      searchbox.addClass('has-value');
    }

    function error() {
      alert('Es war nicht möglich Sie zu lokalisieren.');
    }

    navigator.geolocation.getCurrentPosition(success, error);

  });


