  $.getScript( google_maps_src, function ($) {

  'use strict';

  let map = new google.maps.Map(document.getElementById("map"), {
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    maxZoom: 17
  });

  let bounds = new google.maps.LatLngBounds();

  function placeMarker(location) {
    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(location[1], location[2]),
      title: location[0],
      icon: '//' + window.location.hostname + eval(location[3]),
      map: map
    });
    let infowindow = new google.maps.InfoWindow({
      content: location[4]
    });

    marker.addListener("click", function () {
      infowindow.open(map, marker);
    });

    bounds.extend(marker.position);
  }

  let x = 0;
  for (x = 0; x < locations.length; x++) {
    placeMarker(locations[x]);
  }
  map.fitBounds(bounds);

});
