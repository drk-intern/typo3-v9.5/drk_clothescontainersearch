<?php

// Clothescontainersearch Plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'drk_clothescontainersearch',
    'ClothescontainerSearch',
    'LLL:EXT:drk_clothescontainersearch/Resources/Private/Language/locallang_be.xlf:tt_content.clothescontainersearchform_plugin.title',
    'EXT:drk_clothescontainersearch/Resources/Public/Icons/drk-logo-icon.svg',
    'DRK Kleidercontainer'
);

$GLOBALS['TCA']['tt_content']['types'][$pluginSignature] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types'][$pluginSignature],
    [
        'showitem' => '
            --div--;General,
            --palette--;General;general,
            --palette--;Headers;headers,
            --div--;Einstellungen,
            pi_flexform'
    ]
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    '*',
    // FlexForm configuration schema file
    'FILE:EXT:drk_clothescontainersearch/Configuration/FlexForms/clothescontainersearch.xml',
    // ctype
    'drkclothescontainersearch_clothescontainersearch'
);

// Clothescontainersearch Plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'drk_clothescontainersearch',
    'ClothescontainerSearchResult',
    'LLL:EXT:drk_clothescontainersearch/Resources/Private/Language/locallang_be.xlf:tt_content.clothescontainersearchformresult_plugin.title',
    'EXT:drk_clothescontainersearch/Resources/Public/Icons/drk-logo-icon.svg',
    'DRK Kleidercontainer'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    '*',
    // FlexForm configuration schema file
    'FILE:EXT:drk_clothescontainersearch/Configuration/FlexForms/clothescontainersearchresult.xml',
    // ctype
    'drkclothescontainersearch_clothescontainersearchresult'
);

$GLOBALS['TCA']['tt_content']['types'][$pluginSignature] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types'][$pluginSignature],
    [
        'showitem' => '
            --div--;General,
            --palette--;General;general,
            --palette--;Headers;headers,
            --div--;Einstellungen,
            pi_flexform'
    ]
);
